provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "sg-project2"
  region      = "us-central1"
}